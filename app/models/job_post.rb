class JobPost < ApplicationRecord
  has_many :job_applications, dependent: :destroy

  validates_presence_of :title, :description, :created_by
end
