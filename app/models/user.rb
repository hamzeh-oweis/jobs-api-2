class User < ApplicationRecord
  has_secure_password

  has_many :job_posts, foreign_key: :created_by
  has_many :job_applications, foreign_key: :created_by
  validates_presence_of :name, :email, :password_digest, :is_admin
end