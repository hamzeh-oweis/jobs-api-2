class JobApplication < ApplicationRecord
  belongs_to :job_post

  validates_presence_of :status, :created_by
end
