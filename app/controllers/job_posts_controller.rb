class JobPostsController < ApplicationController
  before_action :set_job_post, only: [:show, :update, :destroy]

  # GET /job_posts
  def index
    @job_posts = JobPost.all # current_user.job_posts
    json_response(@job_posts)
  end

  # POST /job_posts
  def create
    authorize JobPost
    @job_post = current_user.job_posts.create!(job_post_params)
    json_response(@job_post, :created)
  end

  # GET /job_posts/:id
  def show
    json_response(@job_post)
  end

  # PUT /job_posts/:id
  def update
    authorize @job_post
    @job_post.update(job_post_params)
    head :no_content
  end

  # DELETE /job_posts/:id
  def destroy
    authorize @job_post
    @job_post.destroy
    head :no_content
  end

  private

  def job_post_params
    params.permit(:title, :description)
  end

  def set_job_post
    @job_post = JobPost.find(params[:id])
  end
end
