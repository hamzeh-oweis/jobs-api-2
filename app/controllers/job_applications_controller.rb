class JobApplicationsController < ApplicationController
  before_action :set_job_post
  before_action :set_job_application, only: [:show, :update, :destroy]

  # GET /job_posts/:job_post_id/job_applications
  def index
    json_response(@job_post.job_applications)
  end

  # GET /job_posts/:job_post_id/job_applications/:job_application_id
  def show
    json_response(@job_application)
  end

  # POST /job_posts/:job_post_id/job_applications
  def create
    authorize JobApplication
    @new_job_application = @job_post.job_applications.new(job_application_params)
    @new_job_application.created_by = current_user.id
    @new_job_application.save!
    # @job_post.job_applications.create!(job_application_params)
    # @job_application.update(created_by: current_user.id)
    json_response(@job_post.job_applications, :created)
  end

  # PUT /job_posts/:job_post_id/job_applications/:job_application_id
  def update
    authorize @job_application
    @job_application.update(job_application_params)
    head :no_content
  end

  # DELETE /job_posts/:job_post_id/job_applications/:job_application_id
  def destroy
    authorize @job_application
    @job_application.destroy
    head :no_content
  end

  private

  def job_application_params
    params.permit(:status)
  end

  def set_job_post
    @job_post = JobPost.find(params[:job_post_id])
  end

  def set_job_application
    @job_application = @job_post.job_applications.find_by!(id: params[:id]) if @job_post
  end
end
