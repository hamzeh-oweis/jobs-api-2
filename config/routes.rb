Rails.application.routes.draw do
  resources :job_posts do
    resources :job_applications
  end
  post 'auth/login', to: 'authentication#authenticate'
  post 'signup', to: 'users#create'
end
