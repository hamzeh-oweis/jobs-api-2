require 'rails_helper'

RSpec.describe "JobPosts", type: :request do
  # initialize test data
  let(:user) { create(:user, is_admin: 1) }
  let!(:job_posts) { create_list(:job_post, 10, created_by: user.id) }
  let(:valid_job_post_id) { job_posts.first.id }
  let(:invalid_job_post_id) { 100 }

  # authorize request
  let(:headers) { valid_headers }

  # Test suite for GET /job_posts
  describe "GET /job_posts" do
    # make HTTP get request before each example
    before { get '/job_posts', params: {}, headers: headers }
    
    it "returns job posts" do
      expect(json).not_to be_empty
      expect(json.size).to eq(10)
    end

    it "returns status code 200" do
      expect(response).to have_http_status(200)
    end
  end

  # Test suite for GET /job_posts/:id
  describe "GET /job_posts/:id" do
    context "when the record exists" do
      # make HTTP get request using a valid_job_post_id 
      before { get "/job_posts/#{valid_job_post_id}", params: {}, headers: headers}

      it "returns the job post" do
        expect(json).not_to be_empty
        expect(json['id']).to eq(valid_job_post_id)
      end
      
      it "return status code 200" do
        expect(response).to have_http_status(200)
      end
    end

    context "when the record does not exist" do
      # make HTTP get request using an invalid_job_post_id 
      before { get "/job_posts/#{invalid_job_post_id}", params: {}, headers: headers }

      it "returns status code 404" do
        expect(response).to have_http_status(404)
      end

      it "returns a not found message" do
        expect(response.body).to match(/Couldn't find JobPost/)
      end
    end
  end

  # Test suite for POST /job_posts
  describe "POST /job_posts" do
    # valid payload
    let(:valid_attributes) do 
      { title: 'Job Title', description: 'Job one line description' }.to_json
    end
    # invalid payload
    let(:invalid_attributes) do 
      { title: nil, description: 'Job one line description' }.to_json
    end

    context "when the request is valid" do
      before { post '/job_posts', params: valid_attributes, headers: headers }

      it 'creates a job post' do
        expect(json['title']).to eq('Job Title')
      end

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end
      
    context "when the request is invalid" do
      before { post '/job_posts', params: invalid_attributes, headers: headers }
      
      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a validation failure message' do
        expect(response.body)
          .to match(/Validation failed: Title can't be blank/)
      end
    end
  end

  # Test suite for PUT /job_posts/:id
  describe 'PUT /job_posts/:id' do
    let(:valid_attributes) { { title: 'Job Title' }.to_json }

    context 'when the record exists' do
      before { put "/job_posts/#{valid_job_post_id}", params: valid_attributes, headers: headers }

      it 'updates the record' do
        expect(response.body).to be_empty
      end

      it 'returns status code 204' do
        expect(response).to have_http_status(204)
      end
    end
  end
  
  # Test suite for DELETE /job_posts/:id
  describe 'DELETE /job_posts/:id' do
    before { delete "/job_posts/#{valid_job_post_id}", params: {}, headers: headers }

    it 'returns status code 204' do
      expect(response).to have_http_status(204)
    end
  end
end
