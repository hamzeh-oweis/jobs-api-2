require 'rails_helper'

RSpec.describe JobPostPolicy, type: :policy do
  let(:admin_user) { create(:user, is_admin: 1) }
  let(:user) { create(:user, is_admin: 0) }
  let(:job_post) { create(:job_post) }

  subject { described_class }

  # permissions ".scope" do
  #   it "Job posts requested" do
  #     expect(subject).to permit(nil, job_post)
  #   end
  # end

  permissions :index? do
    it "Job posts requested" do
      expect(subject).to permit(nil, job_post)
    end
  end

  permissions :show? do
    it "Job posts requested" do
      expect(subject).to permit(nil, job_post)
    end
  end

  permissions :create? do
    it "Job Seeker" do
      expect(subject).not_to permit(user, job_post)
    end
    it "Admin User" do
      expect(subject).to permit(admin_user, job_post)
    end
  end

  permissions :update? do
    it "Job Seeker" do
      expect(subject).not_to permit(user, job_post)
    end
    it "Admin User" do
      expect(subject).to permit(admin_user, job_post)
    end
  end

  permissions :destroy? do
    it "Job Seeker" do
      expect(subject).not_to permit(user, job_post)
    end
    it "Admin User" do
      expect(subject).to permit(admin_user, job_post)
    end
  end
end