require 'rails_helper'

# Test suite for the JobApplication model
RSpec.describe JobApplication, type: :model do
  # Association test
  # ensure a job application record belongs to a single job post record
  it { should belong_to(:job_post) }
  # Validation test
  # ensure columns status and created_by are present before saving
  it { should validate_presence_of(:status) }
  it { should validate_presence_of(:created_by) }
end